

### <center>Xuefei Zhong</center>
<center>San Diego, California | (626)-765-4139 | felixzhong94@gmail.com</center>



#### Technical Skills

---

*Languages:* Java, JSP, Java Script, PHP, Html, SQL, C, C++, C#, Matlab   
*Technologies:* Ansible, Java EE, Spring, Hibernate, Junit, Selenium, Cucumber, AngularJs, Ui Bootstrap, Leaflet, OWF  
*Software:* Git, Maven, Eclipse, VS Code, Postgresql, Solr, Tomcat, Docker, Nodejs, Jenkins, Jira, Bitbuicket, Confluence  
*Experiences*: Web Development, Agile Developement, Scrum, Web Map Service, AWS, CI, DevOps  
*Operating Systems:* CentOS, RHEL 6, RHEL 7, Windows Server  



#### Professional Experiences

---

ForwardSlope Inc - *Software Engineer*
<span style="float:right">10/15/18 – Present</span>
* Product owner for 3 sperate web applications. Worked with end user and stake holder to define and execute requirement.
* Worked as lead software engineer on web based data analytics software. Led development teams using agile processes and tools
* Architected authorization solution based on the OpenID Connect to improved manageability.


ForwardSlope Inc - *Jr. Software Engineer*
<span style="float:right">10/2/16 - 10/15/18</span>
* Worked with client to refine requirements and developed custom user management application to optimize administration work flow.
* Integrated manual ETL and data processing workflows into Apache Nifi pipelines. Created automated and robust data ingestion processes.
* Created and maintain Ansible scripts to support the automated deployment of system environments, applications, & etl processes.
* Developed and deployed reusable mapping widget to display geo-spatial data across multiple web applications.
* Provided support and updates to legacy JavaEE software.


ForwardSlope Inc - *Computer Engineer Inter*
<span style="float:right">7/25/2016 - 9/30/2016</span>
* Created Selenium Integration test cases to replaced manual testing of web applications.
* Used Cucumber to implement acceptance testing with business-readable specifications.
* Wrote additional Junit test cases for existing code base to achieve 85% code coverage.



#### Education

---
<div>
    University of California, San Diego
    <br>
    Bachelor of Science in Computer Engineering
    <br>
    <small>Provost Honors</small>
    <span style="float:right">Fall 2012 - Fall 2016</span>
</div>



#### Certification

---
<div>
    CompTIA Security+	
    <span style="float:right">6/22/2018</span>  
</div>

<div>
    IAT Level II
    <span style="float:right">10/9/2018</span>
</div>